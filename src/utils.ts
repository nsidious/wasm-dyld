// Copied from Emscripten src/support.js

export function convertJsFunctionToWasm(func: Function, sig: string) {
    // The module is static, with the exception of the type section, which is
    // generated based on the signature passed in.
    let typeSection = [
        0x01, // id: section,
        0x00, // length: 0 (placeholder)
        0x01, // count: 1
        0x60 // form: func
    ]
    for (let i = 0; i < sig.length; i++) {
        let t = sig[i]
        if (["i", "j", "f", "d", "v"].indexOf(t) === -1)
            throw new Error(`invalid parameter type ${sig[i]}`)
    }
    let sigRet = sig.slice(0, 1) as "i" | "j" | "f" | "d" | "v"
    let sigParam = (sig.slice(1) as unknown) as ("i" | "j" | "f" | "d")[]
    let typeCodes = {
        i: 0x7f, // i32
        j: 0x7e, // i64
        f: 0x7d, // f32
        d: 0x7c // f64
    }

    // Parameters, length + signatures
    typeSection.push(sigParam.length)
    for (var i = 0; i < sigParam.length; ++i) {
        let typeCode = typeCodes[sigParam[i]]
        if (typeCode == null)
            throw new Error(`illegal parameter type: ${sigParam[i]}`)
        typeSection.push(typeCode)
    }

    // Return values, length + signatures
    // With no multi-return in MVP, either 0 (void) or 1 (anything else)
    if (sigRet == "v") {
        typeSection.push(0x00)
    } else {
        typeSection = typeSection.concat([0x01, typeCodes[sigRet]])
    }

    // Write the overall length of the type section back into the section header
    // (excepting the 2 bytes for the section id and length)
    typeSection[1] = typeSection.length - 2

    // Rest of the module is static
    var bytes = new Uint8Array(
        [
            0x00,
            0x61,
            0x73,
            0x6d, // magic ("\0asm")
            0x01,
            0x00,
            0x00,
            0x00 // version: 1
        ].concat(typeSection, [
            0x02,
            0x07, // import section
            // (import "e" "f" (func 0 (type 0)))
            0x01,
            0x01,
            0x65,
            0x01,
            0x66,
            0x00,
            0x00,
            0x07,
            0x05, // export section
            // (export "f" (func 0 (type 0)))
            0x01,
            0x01,
            0x66,
            0x00,
            0x00
        ])
    )

    // We can compile this wasm module synchronously because it is very small.
    // This accepts an import (at "e.f"), that it reroutes to an export (at "f")
    var module = new WebAssembly.Module(bytes)
    var instance = new WebAssembly.Instance(module, {
        e: {
            f: func
        }
    })
    var wrappedFunc = instance.exports.f
    return wrappedFunc
}
