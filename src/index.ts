import { DynamicModule } from "./dynobj"
import { convertJsFunctionToWasm } from "./utils"
import { Message } from "./msg"
import { GlobalData } from "./globaldata"
import * as globaldata from './globaldata'
import * as dynobj from './dynobj'
import * as msg from './msg'

export const lib = {
    globaldata,
    dynobj,
    msg
}

export const OBJ_EXTERN = 0xff

export class DynamicLinker {
    dumpTbl() {
        for (let i = 0; i < this.table.length; i++) {
            console.log(`tbl[${i}] = ${this.table.get(i)}`)
        }
    }
    globalData: GlobalData = new GlobalData()
    event(e: Message) {
        if (this.restoring)
            throw new Error("attempt to change state while restoring")
        this.globalData.encodeMsg(e)
    }
    memory: WebAssembly.Memory
    stackPointer = new WebAssembly.Global({ value: "i32", mutable: true })
    table = new WebAssembly.Table({ element: "anyfunc", initial: 0 })
    modules: DynamicModule[] = []
    externSigs: Map<string, string> = new Map()
    externFuncs: Map<string, Function> = new Map()
    gotMap: Map<string, WebAssembly.Global> = new Map()
    restoring = false
    constructor(public isShared = false, memSize = -1) {
        if (isShared) {
            if (memSize == -1)
                throw new Error(
                    `you must specify maximum memory when using shared memory`
                )
            this.memory = new WebAssembly.Memory({
                initial: memSize,
                maximum: memSize,
                shared: true
            } as any)
            this.globalData.growPages(1)
        } else this.memory = new WebAssembly.Memory({ initial: 1 })
        this.stackPointer.value = this.memory.buffer.byteLength - 1
    }
    tableSet(pos: number, elem: any) {
        if (pos >= this.table.length)
            this.table.grow(pos - this.table.length + 1)
        this.table.set(pos, elem)
    }
    addExtern(name: string, sig: string, f: Function) {
        this.externSigs.set(name, sig)
        this.externFuncs.set(name, f)
    }
    isModInstantiating = false
    getExternGot(name: string): WebAssembly.Global {
        let map = this.gotMap.get(name)
        if (map != null) return map
        if (!this.isModInstantiating) this.globalData.lock()
        let global
        try {
            this.restoreMq()
            let idx = this.table.length
            let func = this.externFuncs.get(name)
            let sig = this.externSigs.get(name)
            if (func == null || sig == null) throw new Error()
            let nFunc = convertJsFunctionToWasm(func, sig)
            this.tableSet(idx, nFunc)
            global = new WebAssembly.Global({ value: "i32", mutable: true })
            global.value = idx
            this.gotMap.set(name, global)
            this.event({
                eventType: "gotFunc",
                objectId: OBJ_EXTERN,
                name,
                address: idx
            })
        } finally {
            if (!this.isModInstantiating) this.globalData.unlock()
        }
        return global
    }
    loadDynamic(mod: ArrayBuffer): DynamicModule {
        let obj
        this.isModInstantiating = true
        this.globalData.lock()
        try {
            this.restoreMq()
            let idx = this.modules.length
            obj = new DynamicModule(idx, this, mod)
            this.modules[idx] = obj
        } finally {
            this.globalData.unlock()
            this.isModInstantiating = false
        }
        return obj
    }
    setMemory(mem: WebAssembly.Memory) {
        this.memory = mem
    }
    setMq(globalData: GlobalData) {
        this.globalData = globalData
    }
    restoreMq() {
        this.restoring = true
        let event
        while ((event = this.globalData.decodeMsg()) != null) {
            switch (event.eventType) {
                case "gotFunc":
                    let name = event.name
                    let addr = event.address
                    let objId = event.objectId
                    if (objId == OBJ_EXTERN) {
                        let func = this.externFuncs.get(name)
                        let sig = this.externSigs.get(name)
                        if (func == null || sig == null)
                            throw new Error(`extern not found: ${func} ${sig}`)
                        let wFunc = convertJsFunctionToWasm(func, sig)
                        this.tableSet(addr, wFunc)
                        let gbl = new WebAssembly.Global({
                            value: "i32",
                            mutable: true
                        })
                        gbl.value = addr
                        this.gotMap.set(name, gbl)
                    } else {
                        let obj = this.modules[objId]
                        if (obj == null) throw new Error(`invalid state`)
                        let func = obj.getFunc(name)
                        this.tableSet(addr, func)
                        let gbl = new WebAssembly.Global({
                            value: "i32",
                            mutable: true
                        })
                        gbl.value = addr
                        obj.gotMap.set(name, gbl)
                    }
                    break
                case "moduleCreate":
                    let mod = new DynamicModule(
                        event.id,
                        this,
                        event.binary,
                        event.deps.map(id => this.modules[id]),
                        event
                    )
                    this.modules[event.id] = mod
                    break
            }
        }
        this.restoring = false
    }
}
